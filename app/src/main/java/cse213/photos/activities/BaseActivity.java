package cse213.photos.activities;

import androidx.appcompat.app.AppCompatActivity;

import cse213.photos.db.DatabaseHelper;

public class BaseActivity extends AppCompatActivity {
    protected static DatabaseHelper helper;

    public DatabaseHelper getDatabaseHelper() {
        if(helper == null) {
            helper = new DatabaseHelper(getApplicationContext());
        }
        return helper;
    }
}
