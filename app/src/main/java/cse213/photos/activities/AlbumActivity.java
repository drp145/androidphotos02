package cse213.photos.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import cse213.photos.dao.AlbumDao;
import cse213.photos.dao.PhotoDao;
import cse213.photos.dao.TagDao;
import cse213.photos.model.Album;
import cse213.photos.util.AlertUtiity;
import cse213.photos.util.CollectInput;
import cse213.photos.util.MenuUtility;
import cse213.photos.util.PermissionUtility;

public class AlbumActivity extends BaseActivity {

    private static final String TAG = "AlbumActivity";
    private AlbumDao albumDao;
    private PhotoDao photosDao;
    private TagDao tagDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // CODE TO DROP THE DATABASE AND RECREATE IT FRESH
        // getApplication().deleteDatabase("photosApp");

        setContentView(R.layout.activity_album);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        albumDao = new AlbumDao(getDatabaseHelper());
        photosDao = new PhotoDao(getDatabaseHelper());
        tagDao = new TagDao(getDatabaseHelper());

        PermissionUtility.verifyStoragePermissions(this);

        ArrayAdapter adapter = new ArrayAdapter<Album>(this,
                R.layout.albums_listview, albumDao.getAllAlbums()) {

            public View getView(int position, View convertView, ViewGroup parent) {
                TextView t;

                if (convertView == null) {
                    convertView = LayoutInflater.from(this.getContext())
                            .inflate(R.layout.albums_listview, parent, false);

                    t = (TextView) convertView.findViewById(R.id.albumName);

                    convertView.setTag(t);
                } else {
                    t = (TextView) convertView.getTag();
                }

                Album item = getItem(position);
                if (item != null) {
                    t.setText(item.getName());
                }

                return convertView;
            }
        };

        ListView listView = findViewById(R.id.albums_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Album a = (Album) listView.getAdapter().getItem(position);
            Log.i(TAG, a.getName());

            Intent intent = new Intent(AlbumActivity.this, PhotosActivity.class);
            intent.putExtra("photosList", new ArrayList<>(photosDao.getAllPhotos(a.getId())));
            intent.putExtra("albumName", a.getName());
            intent.putExtra("albumId", a.getId());
            AlbumActivity.this.startActivity(intent);
        });

        listView.setLongClickable(true);
        listView.setOnItemLongClickListener((parent, view, position, id) -> {
            Album selectedAlbum = (Album) parent.getItemAtPosition(position);
            Log.i(TAG, "Selected album: " + selectedAlbum.getName());

            MenuUtility.showMenu(AlbumActivity.this, "Albums Menu",
                    new String[]{"Rename Album", "Delete Album"},
                    new Runnable[]{
                            () -> CollectInput.collectInput(AlbumActivity.this, "Enter new album name", (albumName) -> {
                                if (albumDao.findAlbumByName(albumName) != null) {
                                    CollectInput.showErrorMessage(AlbumActivity.this, "Error",
                                            "An album with this name already exists.");
                                } else {
                                    selectedAlbum.setName(albumName);
                                    albumDao.updateAlbum(selectedAlbum);
                                    adapter.notifyDataSetChanged();
                                }
                            }),
                            () -> {
                                albumDao.deleteAlbum(selectedAlbum);
                                adapter.remove(selectedAlbum);
                                adapter.notifyDataSetChanged();
                            }
                    });

            return true; // true to show the consumption of long click.
        });

        FloatingActionButton addAlbum = findViewById(R.id.addAlbum);
        FloatingActionButton searchPhotos = findViewById(R.id.searchPhotos);
        addAlbum.setOnClickListener(e -> CollectInput.collectInput(this, "Enter album name", (albumName) -> {
            if(albumDao.findAlbumByName(albumName) == null) {
                Album a = albumDao.addAlbum(new Album(albumName));
                adapter.add(a);
            }
            else {
                AlertUtiity.showAlert(this, "Error", "Duplicate album name.");
            }
        }));

        searchPhotos.setOnClickListener(e -> {
            EditText p1 = new EditText(AlbumActivity.this);
            p1.setHint("Person");
            p1.setLines(1);

            EditText p2 = new EditText(AlbumActivity.this);
            p2.setHint("Location");
            p2.setLines(1);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(10, 10, 10, 10);

            LinearLayout layout = new LinearLayout(AlbumActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.addView(p1, layoutParams);
            layout.addView(p2, layoutParams);
            layout.setPaddingRelative(40, 40, 40, 40);

            new AlertDialog.Builder(AlbumActivity.this)
                    .setTitle("Search Photos")
                    .setView(layout)
                    .setPositiveButton("Ok", (dialog, whichButton) -> {
                        Intent intent = new Intent(AlbumActivity.this, PhotosActivity.class);
                        List<Integer> photoIds = new ArrayList<>();
                        if (!p1.getText().toString().isEmpty())
                            photoIds.addAll(tagDao.getAllPhotoIdsWithTags("Person", p1.getText().toString()));
                        if (!p2.getText().toString().isEmpty())
                            photoIds.addAll(tagDao.getAllPhotoIdsWithTags("Location", p2.getText().toString()));
                        intent.putExtra("photosList", new ArrayList<>(photoIds.stream()
                                .map(x -> photosDao.findById(x)).collect(Collectors.toList())));
                        AlbumActivity.this.startActivity(intent);
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        });
    }
}