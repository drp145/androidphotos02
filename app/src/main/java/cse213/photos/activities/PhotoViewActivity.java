package cse213.photos.activities;

import android.app.AlertDialog;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import cse213.photos.dao.PhotoDao;
import cse213.photos.dao.TagDao;
import cse213.photos.model.Photo;
import cse213.photos.model.Tag;
import cse213.photos.util.AlertUtiity;

public class PhotoViewActivity extends BaseActivity {

    private TagDao tagDao;
    private int photoIndex;
    ArrayList<Photo> photosList;
    ImageView image;
    Button next, prev, tags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        photosList = (ArrayList<Photo>) getIntent().getSerializableExtra("photosList");
        photoIndex = getIntent().getIntExtra("photoIndex", photosList.isEmpty() ? -1 : 0);

        setContentView(R.layout.activity_photo_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tagDao = new TagDao(getDatabaseHelper());
        image = findViewById(R.id.image);
        next = findViewById(R.id.next);
        prev = findViewById(R.id.prev);
        tags = findViewById(R.id.tags);
        setPhoto(photoIndex);

        next.setOnClickListener(e -> setPhoto(photoIndex + 1));
        prev.setOnClickListener(e -> setPhoto(photoIndex - 1));
        tags.setOnClickListener(e -> showTags());
    }

    public void showTags() {
        Photo currentPhoto = photosList.get(photoIndex);

        TableLayout table = new TableLayout(this);

        List<Tag> tags = tagDao.getAllTags(currentPhoto.getId());
        for (Tag t : tags) {
            TableRow row = new TableRow(this);
            Consumer<String> addCol = (x) -> {
                TextView tv = new TextView(this);
                tv.setText(x);
                tv.setWidth(250);
                row.addView(tv);
            };
            addCol.accept(t.getTagName());
            addCol.accept(t.getTagValue());
            Button b = new Button(this);
            b.setBackgroundColor(Color.TRANSPARENT);
            b.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_delete, 0, 0, 0);
            b.setOnClickListener(e -> {
                tagDao.deleteTag(t);
                table.removeView(row);
            });
            row.addView(b);
            table.addView(row);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Photo Tags");
        if (!tags.isEmpty()) {
            ScrollView sv = new ScrollView(this);
            sv.addView(table);
            sv.setPaddingRelative(50, 40, 10, 20);
            builder.setView(sv);
        }

        AlertDialog alertDialog = builder.setPositiveButton("Add Tag", (dialog, whichButton) -> addTag(currentPhoto))
                .setNegativeButton("Cancel", null).create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

    private void addTag(Photo currentPhoto) {
        final ArrayAdapter<String> adp = new ArrayAdapter<>(PhotoViewActivity.this,
                android.R.layout.simple_spinner_dropdown_item, new String[]{"Location", "Person"});

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPaddingRelative(70, 70, 70, 40);

        final Spinner sp = new Spinner(PhotoViewActivity.this);
        sp.setLayoutParams(new LinearLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
        sp.setAdapter(adp);

        EditText tagValue = new EditText(PhotoViewActivity.this);
        tagValue.setHint("Tag Value");
        tagValue.setLines(1);

        layout.addView(sp);
        layout.addView(tagValue);
        new AlertDialog.Builder(PhotoViewActivity.this)
                .setTitle("Adding photo tag")
                .setView(layout)
                .setPositiveButton("Confirm", (dialog, whichButton) -> {
                    if (!tagValue.getText().toString().isEmpty()) {
                        // Add the tag on the dao.
                        tagDao.addTag(new Tag(currentPhoto.getId(), sp.getSelectedItem().toString(), tagValue.getText().toString()));
                        showTags();
                    }
                })
                .setNegativeButton("Cancel", null).show();
    }

    public void setPhoto(int index) {
        if(index < 0 || index >= photosList.size()) {
            tags.setEnabled(false);
            prev.setEnabled(false);
            next.setEnabled(false);
            AlertUtiity.showAlert(PhotoViewActivity.this, "Alert", "No Images found.");
            return;
        }

        photoIndex = index;
        Photo p = photosList.get(photoIndex);
        image.setImageBitmap(BitmapFactory.decodeFile(p.getFilePath()));
        if (photoIndex >= photosList.size() - 1) {
            next.setEnabled(false);
        } else {
            next.setEnabled(true);
        }

        if (photoIndex <= 0) {
            prev.setEnabled(false);
        } else {
            prev.setEnabled(true);
        }
        tags.setEnabled(true);
    }
}