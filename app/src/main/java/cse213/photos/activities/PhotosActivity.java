package cse213.photos.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import cse213.photos.dao.AlbumDao;
import cse213.photos.dao.PhotoDao;
import cse213.photos.model.Album;
import cse213.photos.model.Photo;
import cse213.photos.util.AlertUtiity;
import cse213.photos.util.MenuUtility;

public class PhotosActivity extends BaseActivity {

    private PhotoDao photosDao;
    private AlbumDao albumDao;
    GridView gridView;
    ArrayList<Photo> photosList;
    FloatingActionButton addPhoto;
    public static final int PICK_IMAGE_ACTION = 100;
    BaseAdapter photosAdapter;
    int albumId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        photosList = (ArrayList<Photo>) getIntent().getSerializableExtra("photosList");
        String albumName = getIntent().getStringExtra("albumName");
        albumId = getIntent().getIntExtra("albumId", -1);
        if(albumName == null || albumName.isEmpty()) {
            albumName = "Search Results";
        }

        if(photosList.isEmpty()) {
            AlertUtiity.showAlert(PhotosActivity.this, "Alert", "No Images found.");
        }

        setContentView(R.layout.activity_photos);
        photosDao = new PhotoDao(getDatabaseHelper());
        albumDao = new AlbumDao(getDatabaseHelper());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(toolbar.getTitle().toString() + " (" + albumName + ")");

        gridView = findViewById(R.id.photosGrid);
        addPhoto = findViewById(R.id.addPhoto);
        if(albumId == -1) {
            // For search results, no addition to album
            addPhoto.setVisibility(View.INVISIBLE);
        }

        photosAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return photosList.size();
            }

            @Override
            public Object getItem(int position) {
                return photosList.get(position);
            }

            @Override
            public long getItemId(int position) {
                return photosList.get(position).getId();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView ==  null) {
                    convertView = LayoutInflater.from(PhotosActivity.this).inflate(R.layout.photos_gridview, parent, false);
                }

                ImageView imageView;
                imageView = convertView.findViewById(R.id.image);
                imageView.setImageBitmap(BitmapFactory.decodeFile(photosList.get(position).getFilePath()));
                return convertView;
            }
        };

        gridView.setAdapter(photosAdapter);
        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Photo p = (Photo) parent.getItemAtPosition(position);
            Log.i("Selected photo: ", p.getId() + " " + p.getFilePath() + " " + p.getCaption());

            Intent intent = new Intent(PhotosActivity.this, PhotoViewActivity.class);
            intent.putExtra("photosList", photosList);
            intent.putExtra("photoIndex", photosList.indexOf(p));
            PhotosActivity.this.startActivity(intent);
        });

        gridView.setOnItemLongClickListener((parent, view, position, id) -> {
            Photo p = (Photo) parent.getItemAtPosition(position);

            MenuUtility.showMenu(PhotosActivity.this, "Photo Menu",
                    new String[] {"Move Photo", "Delete Photo"},
                    new Runnable[] {
                            () -> {
                                // Start album intent with a option parameter to move the photoId.
                                List<Album> albums = albumDao.getAllAlbums();
                                String[] albumNames = albums.stream().map(Album::getName).toArray(String[]::new);
                                MenuUtility.showMenu(PhotosActivity.this, "Select Album", albumNames, (albumIndex) -> {
                                    int destAlbumId = albums.get(albumIndex).getId();
                                    if(p.getAlbumId() != destAlbumId) {
                                        p.setAlbumId(destAlbumId);
                                        photosDao.updatePhoto(p);
                                        photosList.remove(p);
                                        photosAdapter.notifyDataSetChanged();
                                    }
                                });
                            },
                            () -> new AlertDialog.Builder(PhotosActivity.this)
                                .setTitle("Delete")
                                .setMessage("Do you want to delete this photo?")
                                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                                    photosDao.deletePhoto(p);
                                    photosList.remove(p);
                                    photosAdapter.notifyDataSetChanged();
                                })
                                .setNegativeButton(android.R.string.no, null).show()
                    });
            return true;
        });

        addPhoto.setOnClickListener(e -> {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select image"), PICK_IMAGE_ACTION);
        });
    }

    public static String getPath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            try {
                Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
                cursor.close();
            } catch (Exception ignored) { }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_ACTION && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String path = getPath(this, data.getData());
                Log.i("Added photo: ", path);
                Photo p = new Photo(path);
                p.setCaption(path);
                p.setAlbumId(albumId);
                photosList.add(photosDao.addPhoto(p));
                photosAdapter.notifyDataSetChanged();
            }
        }
    }

}