package cse213.photos.dao;

public abstract class GenericDao {
    public abstract String getCreateStatement();

    public abstract String getTableName();
}
