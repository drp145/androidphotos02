package cse213.photos.dao;

import android.content.ContentValues;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;

import cse213.photos.db.DatabaseHelper;
import cse213.photos.model.Photo;
import cse213.photos.model.Tag;

public class PhotoDao extends GenericDao {
    DatabaseHelper dbHelper;

    private static String TABLE_NAME = "photo";
    private static String ID_COL = "id";
    private static String CAPTION_COL = "caption";
    private static String FILE_PATH_COL = "filePath";
    private static String ALBUM_ID_COL = "albumId";

    public PhotoDao(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CAPTION_COL + " TEXT, " +
                FILE_PATH_COL + " TEXT, " +
                ALBUM_ID_COL + " INTEGER)";
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    public void deletePhoto(Photo p) {
        // Delete all tags as well.
        TagDao tagDao = new TagDao(dbHelper);
        for (Tag t : tagDao.getAllTags(p.getId())) {
            tagDao.deleteTag(t);
        }
        dbHelper.getSqlite().delete(TABLE_NAME, ID_COL + "=?", new String[]{p.getId() + ""});
    }

    public Photo addPhoto(Photo photo) {
        ContentValues values = new ContentValues();

        values.put(CAPTION_COL, photo.getCaption());
        values.put(FILE_PATH_COL, photo.getFilePath());
        values.put(ALBUM_ID_COL, photo.getAlbumId());
        long photoId = dbHelper.getSqlite().insert(TABLE_NAME, null, values);

        return findById((int) photoId);
    }

    private Photo getPhotoFromCursor(Cursor cursor) {

        int id = cursor.getInt((cursor.getColumnIndex(ID_COL)));
        String caption = cursor.getString((cursor.getColumnIndex(CAPTION_COL)));
        String filePath = cursor.getString((cursor.getColumnIndex(FILE_PATH_COL)));
        int albumId = cursor.getInt((cursor.getColumnIndex(ALBUM_ID_COL)));

        Photo p = new Photo(filePath);
        p.setId(id);
        p.setCaption(caption);
        p.setFilePath(filePath);
        p.setAlbumId(albumId);

        return p;
    }

    public Photo findById(int photoId) {
        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, CAPTION_COL, FILE_PATH_COL, ALBUM_ID_COL},
                ID_COL + " = ?", new String[]{photoId + ""}, null, null, null);

        if (cursor.moveToFirst()) {
            return getPhotoFromCursor(cursor);
        }
        return null;
    }

    public List<Photo> getAllPhotos(int albumId) {
        List<Photo> photos = new ArrayList<Photo>();

        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, CAPTION_COL, FILE_PATH_COL, ALBUM_ID_COL},
                ALBUM_ID_COL + " = ?", new String[]{albumId + ""}, null, null, ID_COL);

        if (cursor.moveToFirst()) {
            do {
                photos.add(getPhotoFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        return photos;
    }

    public void updatePhoto(Photo photo) {
        ContentValues values = new ContentValues();
        values.put(CAPTION_COL, photo.getCaption());
        values.put(FILE_PATH_COL, photo.getFilePath());
        values.put(ALBUM_ID_COL, photo.getAlbumId());
        dbHelper.getSqlite().update(TABLE_NAME, values, ID_COL + "=?", new String[]{"" + photo.getId()});
    }
}
