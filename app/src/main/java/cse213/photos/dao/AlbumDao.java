package cse213.photos.dao;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import cse213.photos.db.DatabaseHelper;
import cse213.photos.model.Album;
import cse213.photos.model.Photo;

public class AlbumDao extends GenericDao {
    DatabaseHelper dbHelper;

    private static String TABLE_NAME = "album";
    private static String ID_COL = "id";
    private static String NAME_COL = "name";

    public AlbumDao(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NAME_COL + " TEXT)";
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    public Album addAlbum(Album album) {
        ContentValues values = new ContentValues();
        values.put(NAME_COL, album.getName());
        long albumId = dbHelper.getSqlite().insert(TABLE_NAME, null, values);
        return findAlbumById((int) albumId);
    }

    private Album getAlbumFromCursor(Cursor cursor) {
        int id = cursor.getInt((cursor.getColumnIndex(ID_COL)));
        String name = cursor.getString((cursor.getColumnIndex(NAME_COL)));

        Album a = new Album(name);
        a.setId(id);

        return a;
    }

    public Album findAlbumById(int albumId) {
        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, NAME_COL},
                ID_COL + " = ?", new String[]{albumId + ""}, null, null, null);

        if (cursor.moveToFirst()) {
            return getAlbumFromCursor(cursor);
        }
        return null;
    }

    public void updateAlbum(Album album) {
        ContentValues con = new ContentValues();
        con.put(NAME_COL, album.getName());
        dbHelper.getSqlite().update(TABLE_NAME, con, ID_COL + "=?",new String[]{""+album.getId()});
    }

    public Album findAlbumByName(String albumName) {
        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, NAME_COL},
                NAME_COL + " = ?", new String[]{albumName}, null, null, null);

        if (cursor.moveToFirst()) {
            return getAlbumFromCursor(cursor);
        }
        return null;
    }

    public void deleteAlbum(Album a) {
        // Remove all the photos in the album.
        PhotoDao photoDao = new PhotoDao(dbHelper);
        for(Photo p: photoDao.getAllPhotos(a.getId())) {
            photoDao.deletePhoto(p);
        }
        dbHelper.getSqlite().delete(TABLE_NAME, ID_COL + "=?", new String[] { a.getId() + "" }) ;
    }

    public List<Album> getAllAlbums() {
        List<Album> albums = new ArrayList<Album>();

        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, NAME_COL},
                null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                albums.add(getAlbumFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        return albums;
    }
}
