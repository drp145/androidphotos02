package cse213.photos.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cse213.photos.db.DatabaseHelper;
import cse213.photos.model.Album;
import cse213.photos.model.Photo;
import cse213.photos.model.Tag;

public class TagDao extends GenericDao {
    DatabaseHelper dbHelper;

    private static String TABLE_NAME = "tag";
    private static String ID_COL = "id";
    private static String PHOTO_ID_COL = "photoId";
    private static String TAG_NAME_COL = "tagName";
    private static String TAG_VALUE_COL = "tagValue";

    public TagDao(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public String getCreateStatement() {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PHOTO_ID_COL + " INTEGER, " +
                TAG_NAME_COL + " TEXT, " +
                TAG_VALUE_COL + " TEXT)";
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    public void addTag(Tag tag) {
        ContentValues values = new ContentValues();
        values.put(PHOTO_ID_COL, tag.getPhotoId());
        values.put(TAG_NAME_COL, tag.getTagName());
        values.put(TAG_VALUE_COL, tag.getTagValue());
        dbHelper.getSqlite().insert(TABLE_NAME, null, values);
    }

    private Tag getTagFromCursor(Cursor cursor) {
        int tagId = cursor.getInt((cursor.getColumnIndex(ID_COL)));
        int photoId = cursor.getInt((cursor.getColumnIndex(PHOTO_ID_COL)));
        String name = cursor.getString((cursor.getColumnIndex(TAG_NAME_COL)));
        String value = cursor.getString((cursor.getColumnIndex(TAG_VALUE_COL)));
        Tag t = new Tag(photoId, name, value);
        t.setId(tagId);
        return t;
    }

    public List<Tag> getAllTags(int photoId) {
        List<Tag> tags = new ArrayList<Tag>();

        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, PHOTO_ID_COL, TAG_NAME_COL, TAG_VALUE_COL},
                PHOTO_ID_COL + "=?", new String[]{photoId + ""}, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                tags.add(getTagFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        return tags;
    }

    public List<Integer> getAllPhotoIdsWithTags(String tagName, String tagValue) {
        Log.i("Tag", "Finding for " + tagName + ", " + tagValue);
        List<Integer> photoIds = new ArrayList<>();

        Cursor cursor = dbHelper.getSqlite().query(TABLE_NAME, new String[]{ID_COL, PHOTO_ID_COL, TAG_NAME_COL, TAG_VALUE_COL},
                TAG_NAME_COL + "=? AND " + TAG_VALUE_COL + " LIKE ?", new String[]{tagName, tagValue + "%"}, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                photoIds.add(cursor.getInt(cursor.getColumnIndex(PHOTO_ID_COL)));
            } while (cursor.moveToNext());
        }

        return photoIds;
    }

    public void deleteTag(Tag t) {
        dbHelper.getSqlite().delete(TABLE_NAME, ID_COL + "=?", new String[]{t.getId() + ""});
    }
}
