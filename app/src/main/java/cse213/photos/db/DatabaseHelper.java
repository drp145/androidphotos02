package cse213.photos.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import cse213.photos.dao.AlbumDao;
import cse213.photos.dao.PhotoDao;
import cse213.photos.dao.TagDao;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = DatabaseHelper.class.getName();

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "photosApp";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        Log.i(LOG, "Setting up database");
        db.execSQL(new AlbumDao(this).getCreateStatement());
        db.execSQL(new PhotoDao(this).getCreateStatement());
        db.execSQL(new TagDao(this).getCreateStatement());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        Log.i(LOG, "Upgrading the database");
        db.execSQL("DROP TABLE IF EXISTS " + new AlbumDao(this).getTableName());
        db.execSQL("DROP TABLE IF EXISTS " + new PhotoDao(this).getTableName());
        db.execSQL("DROP TABLE IF EXISTS " + new TagDao(this).getTableName());

        // create new tables
        onCreate(db);
    }

    public SQLiteDatabase getSqlite() {
        return this.getWritableDatabase();
    }
}