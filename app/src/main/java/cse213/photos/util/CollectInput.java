package cse213.photos.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.function.Consumer;

public class CollectInput {
    public static void collectInput(Context context, String message, Consumer<String> callback) {
        EditText input = new EditText(context);
        input.setLines(1);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(10, 10, 10, 10);

        LinearLayout layout = new LinearLayout(context);
        layout.addView(input, layoutParams);
        layout.setPaddingRelative(40,0,40,0);

        new AlertDialog.Builder(context)
            .setTitle(message)
            .setView(layout)
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    callback.accept(input.getText().toString());
                }
            })
            .setNegativeButton("Cancel", null)
            .show();
    }

    public static void showErrorMessage(Context context, String title, String message) {
        new AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ok", null)
            .show();
    }
}
