package cse213.photos.util;

import android.app.AlertDialog;
import android.content.Context;

public class AlertUtiity {
    public static void showAlert(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).setPositiveButton("Ok", null).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.show();
    }
}
