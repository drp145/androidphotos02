package cse213.photos.util;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.function.Consumer;

public class MenuUtility {

    public static void showMenu(Context context, String title, String menu[], Runnable runnable[]) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle(title);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(menu);

        builderSingle.setNegativeButton("Cancel", null);

        builderSingle.setAdapter(arrayAdapter, (dialog, optionIndex) -> runnable[optionIndex].run());
        builderSingle.show();
    }

    public static void showMenu(Context context, String title, String menu[], Consumer<Integer> callback) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle(title);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
        arrayAdapter.addAll(menu);

        builderSingle.setNegativeButton("Cancel", null);

        builderSingle.setAdapter(arrayAdapter, (dialog, optionIndex) -> callback.accept(optionIndex));
        builderSingle.show();
    }
}
