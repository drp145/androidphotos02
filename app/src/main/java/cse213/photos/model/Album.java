package cse213.photos.model;

import java.util.Objects;

/**
 * Model Class representing an album. An album has a title.
 *
 * @author Dev Patel and Eric Chan
 */
public class Album {
    private int id;
    private String name;

    /**
     * Constructor
     * @param name: name for the album
     */
    public Album(String name) {
        this.name = name;
    }

    /**
     * Get title of album
     * @return Album name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the title of the album
     * @param name Name to be set for the album
     */
    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album p = (Album) o;
        return id == p.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
