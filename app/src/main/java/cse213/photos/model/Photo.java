package cse213.photos.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Model Class representing a photo. A Photo has a caption, url and some set of
 * photo tags along with a photo capture date.
 *
 * @author Dev Patel and Eric Chan
 */
public class Photo implements Serializable {
    private int id;
    private String caption;
    private String filePath;
    private int albumId;

	public static final String LOCATION_TAG = "Location";
	public static final String PERSON_TAG = "Person";
    /**
     * Constructor to create a photo with no tags and from a given file path.
     *
     * @param filePath Path from which photo should be picked
     */
    public Photo(String filePath) {
        this.filePath = filePath;
        this.caption = filePath;
    }

    /**
     * Create a photo from other photo. the tags are also copied for the photo along
     * with path and caption
     *
     * @param other Photo to create a copy from
     */
    public Photo(Photo other) {
        this.filePath = other.filePath;
        this.caption = other.caption;
    }

    /**
     * Get photo caption
     *
     * @return photo caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Get image file path on the disk
     *
     * @return image file path on the disk
     */
    public String getFilePath() {
        return filePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo p = (Photo) o;
        return id == p.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
